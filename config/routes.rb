Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get "/auth", to: "authentication#index"
  get "/auth/callback", to: "authentication#create"

  resources :athletes, only: [] do 
    resources :activities, only: [:index]
  end

  resources :analytics, only: [:index]
end
