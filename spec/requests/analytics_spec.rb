require 'rails_helper'

RSpec.describe "Analytics", type: :request do
  let!(:athlete){ create(:athlete) }

  describe "GET /index" do
    context ":group params" do
      context "when :group param is :month" do
        before :each do
          get "/analytics?group=month"
          @json_response = JSON.parse(response.body)
        end

        it "should show Activity data grouped into months" do
          expect(@json_response["data"].count).to eq(12)
        end

        it "should show total distance and total time spent for that month" do
          month_data = @json_response["data"]["2022-01"]

          expect(month_data).to have_key("total_distance")
          expect(month_data).to have_key("total_time")
        end
      end
    end
  end
end
