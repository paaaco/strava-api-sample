require 'rails_helper'

RSpec.describe "/activities", type: :request do
  let!(:athlete){ create(:athlete) }

  describe "GET /index" do
    before :each do
      (1..5).each do |index|
        create(:activity, name: "Activity ##{index}", athlete: athlete)
      end

      get athlete_activities_url(athlete_id: athlete.id)
    end

    it "should show all activities for the given athlete as a JSON" do
      parsed_body = JSON.parse(response.body)

      expect(parsed_body.size).to eq(athlete.activities.size)
    end
  end
end
