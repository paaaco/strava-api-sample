class Activity < ApplicationRecord
  belongs_to :athlete

  def self.build_activity(activity, athlete)
    @activity = Activity.where(uid: activity["id"]).first_or_initialize
    return nil unless @activity.new_record?

    @activity.assign_attributes(
      name: activity["name"],
      distance: activity["distance"],
      moving_time: activity["moving_time"],
      elapsed_time: activity["elapsed_time"],
      total_elevation_gain: activity["total_elevation_gain"],
      activity_type: activity["type"],
      start_at: activity["start_date"],
      end_at: activity["end_date"],
      max_speed: activity["max_speed"],
      average_heartrate: activity["average_heartrate"],
      max_heartrate: activity["max_heartrate"],
      athlete: athlete
    )
    @activity
  end
end
