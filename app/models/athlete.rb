class Athlete < ApplicationRecord
  has_many :activities

  def self.create_athlete(json_string)
    response = JSON.parse(json_string)

    athlete_data = response["athlete"]

    @athlete = Athlete.where(uid: athlete_data["id"]).first_or_initialize
    @athlete.assign_attributes(
      first_name: athlete_data["firstname"],
      last_name: athlete_data["lastname"],
      access_token_expires_at: Time.at(response["expires_at"]),
      refresh_token: response["refresh_token"],
      access_token: response["access_token"]
    )

    @athlete.save

    return @athlete
  end

  def refresh_token!
    return if DateTime.now < self.access_token_expires_at
    credentials = Rails.application.credentials.strava

    oauth_token_url = URI("https://www.strava.com/api/v3/oauth/token")
    parameters = {
      client_id: credentials["client_id"],
      client_secret: credentials["client_secret"],
      refresh_token: self.refresh_token,
      grant_type: "refresh_token"
    }

    response = Net::HTTP.post_form oauth_token_url, parameters
    
    json_response = JSON.parse response.body
    
    self.update!(
      access_token_expires_at: Time.at(json_response["expires_at"]),
      access_token: json_response["access_token"]
    )
  end
end
