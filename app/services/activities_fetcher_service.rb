class ActivitiesFetcherService 
  def initialize(athlete)
    @athlete = athlete
    @athlete.refresh_token!
  end

  def call
    activities = fetch_activities
    activities.map! do |activity|
      @activity = Activity.build_activity(activity, @athlete)
    end

    Activity.import activities.reject(&:nil?)
  end

  private
  def fetch_activities(page=1, activities=[])
    @activities = activities
    activities_uri = URI("https://www.strava.com/api/v3/activities")
    parameters = {
      page: page,
      access_token: @athlete.access_token,
      per_page: 20
    }
    activities_uri.query = URI.encode_www_form(parameters)

    response = Net::HTTP.get_response(activities_uri)
    json_response = JSON.parse response.body

    if json_response.size > 0
      @activities << json_response
      fetch_activities(page + 1, @activities)
    else
      return @activities.flatten
    end
  end
end