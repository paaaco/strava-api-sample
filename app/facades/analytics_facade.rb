class AnalyticsFacade
  attr_reader :athlete, :activities, :params
  
  def initialize(athlete:, params: { year: Time.now.year, grouping: :month })
    @athlete = athlete
    @activities = athlete.activities
    @params = params
  end

  def aggregated_activities
    hash = {}
    total_distances = total_distance
    total_times = total_time

    (1..12).each do |month|
      year_month_string = "#{params[:year]}-#{month.to_s.rjust(2, "0")}"

      hash[year_month_string] = {
        total_distance: total_distances[year_month_string],
        total_time: total_times[year_month_string]
      }
    end

    hash
  end

  private
  def filtered_activities
    activities.group(grouping)
  end

  def grouping
    if params[:grouping] == :month
      "to_char(start_at, 'YYYY-MM')"
    elsif params[:grouping] == :year
      "to_char(start_at, 'YYYY')"
    end
  end

  def total_distance
    filtered_activities.sum("distance")
  end

  def total_time
    filtered_activities.sum("elapsed_time")
  end
end
