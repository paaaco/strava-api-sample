class ActivitiesController < ApplicationController
  before_action :set_athlete

  def index
    render json: Activity.where(athlete: @athlete).order(start_at: :desc)
  end

  private 
  def set_athlete
    @athlete = Athlete.find(params[:athlete_id])
  end
end
