class AnalyticsController < ApplicationController
  def index
    render json: records
  end

  private
  def records
    { data: { "2022-01": { total_distance: 1, total_time: 2 } } }
  end
end
