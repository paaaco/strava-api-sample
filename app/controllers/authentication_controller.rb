class AuthenticationController < ApplicationController
	require 'net/http'

  def index
    @credentials = Rails.application.credentials.strava

		redirect_uri = "#{request.base_url}/auth/callback"
		parameters = {
			client_id: @credentials["client_id"],
			response_type: "code",
			redirect_uri: "#{redirect_uri}",
			scope: "activity:read_all,profile:read_all",
			approval_prompt: "force"
		}
		
		oauth_url = URI::HTTPS.build(host: 'strava.com', path: '/oauth/authorize', query: parameters.to_query)

		redirect_to oauth_url.to_s, allow_other_host: true
  end

  def create
    @credentials = Rails.application.credentials.strava

		if params[:code]
			code = params[:code]
			parameters = {
				client_id: @credentials["client_id"],
				client_secret: @credentials["client_secret"],
				code: code,
				grant_type: "authorization_code"
			}
			oauth_token_url = URI("https://www.strava.com/api/v3/oauth/token")
			response = Net::HTTP.post_form oauth_token_url, parameters
			
			@athlete = Athlete.create_athlete(response.body)
			render json: @athlete
		end
  end
end
