class CreateAthletes < ActiveRecord::Migration[7.0]
  def change
    create_table :athletes do |t|
      t.bigint :uid
      t.string :first_name
      t.string :last_name
      t.string :access_token
      t.string :refresh_token
      t.datetime :access_token_expires_at

      t.timestamps
    end
  end
end
