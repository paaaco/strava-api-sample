class CreateActivities < ActiveRecord::Migration[7.0]
  def change
    create_table :activities do |t|
      t.string :name
      t.float :distance
      t.integer :moving_time
      t.integer :elapsed_time
      t.integer :total_elevation_gain
      t.string :activity_type
      t.string :uid
      t.datetime :start_at
      t.datetime :end_at
      t.float :max_speed
      t.integer :average_heartrate
      t.integer :max_heartrate
      t.references :athlete

      t.timestamps
    end
  end
end
